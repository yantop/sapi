sapi
========
sapi是免费开源API接口管理系统
基于thinkphp开发
项目介绍
========
**项目截图** 
<img src="./db/screenshot/1.png">
<img src="./db/screenshot/2.png">
<img src="./db/screenshot/3.png">
<img src="./db/screenshot/4.png">
<img src="./db/screenshot/5.png">
<img src="./db/screenshot/6.png">
使用
========
代码拉下来，在根目录composer update
将db下的sql文件导入数据库中
配置虚拟机到public下，重启apache
欢迎大家使用
========
有问题欢迎评论反馈
