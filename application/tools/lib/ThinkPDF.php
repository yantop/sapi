<?php
namespace app\tools\lib;

vendor('TCPDF');
class ThinkPDF extends \TCPDF
{
    /**
     * 页眉
     */
    public function header($title = 'hello',$logo = false){
        //设置logo
        $config = config('pdf');
        if($logo){
            $image_file = $config['logo'];
            $this->Image($image_file, 10, 10, 15, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        }
        // 设置字体
        $this->setTTFFont();
        // 标题
        $this->Cell(0, 15, $title, 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }
    public function setting($creator = 'yantop',$author = 'yantop',$title = 'my first pdf',$subject = 'yantop',$keywords = ['first','pdf']){
        $this->SetCreator($creator);
        $this->SetAuthor($author);
        $this->SetTitle($title);
        $this->SetSubject($subject);
        $this->SetKeywords(implode(',',$keywords));
    }
    /**
     * 页脚
     */
    public function footer($text = 'page'){
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // 设置字体
        $this->setTTFFont();
        // Page number
        $this->Cell(0, 10, $text.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    public function setTTFFont(){
        //var_dump($this->config['ext'].'examples/lang/chi.php');exit;
        $config = config('pdf');
        if (@file_exists($config['ext'].'examples/lang/chi.php')) {
            require($config['ext'].'examples/lang/chi.php');
            $this->setLanguageArray($l);
        } 
        // 设置字体，如果要支持中文 则选择支持中文的字体  
        //var_dump(config('pdfext').'fonts'.DS.'DroidSansFallback.ttf');exit;
        $fontname = \TCPDF_FONTS::addTTFfont($config['ext'].'fonts/DroidSansFallback.ttf', 'TrueTypeUnicode', '', 32);    
        $this->SetFont($fontname, '', 10);
    }
    // 添加标准表格
    public function ColoredTable($header,$data) {
        // Colors, line width and bold font
        $this->SetFillColor(255, 0, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(0.3);
        $this->SetFont('', 'B');
        // Header
        $w = array(40, 35, 40, 45);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = 0;
        foreach($data as $row) {
            $this->Cell($w[0], 6, $row[0], 'LR', 0, 'L', $fill);
            $this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
            $this->Cell($w[2], 6, number_format($row[2]), 'LR', 0, 'R', $fill);
            $this->Cell($w[3], 6, number_format($row[3]), 'LR', 0, 'R', $fill);
            $this->Ln();
            $fill=!$fill;
        }
        $this->Cell(array_sum($w), 0, '', 'T');
    }
}