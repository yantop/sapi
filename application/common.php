<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

use think\Response;

/**
 * ajax数据返回，规范格式
 * @param array $data   返回的数据，默认空数组
 * @param string $msg   信息
 * @param string $status success|error 操作成功|操作失败
 * @param int $code     错误码，0-未出现错误|其他出现错误
 * @param array $extend 扩展数据
 */
function ajax_return($data = [], $msg = "操作成功",$status="success", $code = 0, $extend = [])
{
    $ret = ["code" => $code,'status'=>$status, "msg" => $msg, "data" => $data];
    $ret = array_merge($ret, $extend);

    return Response::create($ret, 'json');
}
/**
 * [create_password 创建用户密码]
 * @param  [string] $password [密码]
 * @param  [string] $salt     [salt]
 * @return [type]           [string]
 */
function create_password($password,$salt){
    $password = md5(md5($password.$salt,true));
    return $password;
}

/**
 * [random_code 生成指定长度的字符串,如果是MD5是true,则生成32位]
 * @param  [type]  $length [长度]
 * @param  boolean $md5    [是否MD5]
 * @return [type]          [字符串]
 */
function random_code($length,$md5 = false,$upper = false){
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $str = '';
    for($i=0;$i<$length;$i++){
        $str .= $chars[mt_rand(0,61)];
    }
    if($md5){
        $str = md5($str.time());
    }
    $str = $upper?strtoupper($str):strtolower($str);
    return $str;
}