<?php
namespace app\apiweb\controller;
use think\Controller;
/**
* 
*/
class Index extends Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->oclassify = new \app\apiweb\model\Classify();
    }

    function index(){
        $projects = $this->oclassify->where(['pid'=>'0'])->order('addtime asc')->select();
        if(!empty($projects)){
            foreach ($projects as &$project) {
                $project = $project->toArray();
                $categories = $this->oclassify->where(['pid'=>$project['id']])->order('addtime asc')->select();
                if(!empty($categories)){
                    foreach ($categories as $category) {
                        $category = $category->toArray();
                        $project['categories'][] = $category;
                    }
                }
            }
        }
        $this->assign('projects',$projects);
        return $this->view->fetch();
    }

    function apilist($proid = null,$catid = null){
        $obj_apilist = new \app\apiweb\model\Apilist();
        $obj_detail  = new \app\apiweb\model\Apidetail();
        $obj_param   = new \app\apiweb\model\Apiparam();

        //获取项目信息及分类信息
        $project = $this->oclassify->where(['id'=>$proid,'pid'=>'0'])->find();
        if(empty($project)){
            $this->error('未查询到该项目信息');
        }
        $project = $project->toArray();
        $categories = $this->oclassify->where(['pid'=>$project['id']])->order('addtime asc')->select();
        $project['apis'] = [];
        $project['categories'] = [];
        if(!empty($categories)){
            foreach ($categories as $category) {
                $category = $category->toArray();
                if($catid == '0'){
                    $catid = $category['id'];
                }
                if($catid == $category['id']){
                    $category['active'] = 'true';
                    
                }else{
                    $category['active'] = 'false';
                }
                
                $project['categories'][] = $category;
            }
        }
        if($catid != '0'){
            $apilist = $obj_apilist->where(['classify'=>$catid])->select();
            if(!empty($apilist)){
                $api_ids = [];
                $tmp_apis = [];
                foreach ($apilist as $oneapi) {
                    $oneapi = $oneapi->toArray();
                    $api_ids[] = $oneapi['id'];
                    $tmp_apis[$oneapi['id']]['apiname'] = $oneapi['apiname'];
                }
                $apis = $obj_detail->where(['listid'=>['in',$api_ids],'status'=>'2'])->order('ctime asc')->select();
                foreach ($apis as &$api) {
                    $api = $api->toArray();
                    $api['apiname'] = $tmp_apis[$api['listid']]['apiname'];
                    $param = $obj_param->where('detailid',$api['id'])->find();
                    $param = $param->toArray();
                    $param['type'] = $obj_param->apiType[$param['type']];
                    $param['header']     = json_decode($param['header'],true);
                    $param['request']    = json_decode($param['request'],true);
                    $param['response']   = json_decode($param['response'],true);
                    $param['statuscode'] = json_decode($param['statuscode'],true);
                    $api['param'] = $param;
                    $project['apis'][] = $api;
                }
            }
        }
        
        //echo "<pre>";print_r($project);exit;
        $this->assign('project',$project);
        return $this->view->fetch();
    }
    
}