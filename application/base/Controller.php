<?php
namespace app\base;
use think\Session;
/**
* 
*/
class Controller extends \think\Controller
{
    
    function __construct()
    {
        parent::__construct();
        $username = Session::get('uname');
        if(!empty($username)){
            $this->assign('username',$username);
        }else{
            $this->redirect('/login.html');
        }
        $obj_member = new \app\admin\model\AdminMembers();
        $user_info = $obj_member->get_user($username);
        $user_auth = unserialize($user_info['group']['auth']);
        //echo "<pre>";print_r($user_auth);exit;
        if(empty($user_info)){
            $this->error('未查询到该用户信息','/login.html');
        }
        $this->user = $user_info;
        $path_info = $this->request->pathinfo();
        //echo "<pre>";print_r($path_info);exit;
        $this->onode = new \app\admin\model\AdminNodes();
        
        //路径
        $paths = $this->onode->get_path($path_info);
        if(!empty($paths)&&!in_array($paths['0']['auth'],$user_auth)&&!$path_info!='admin/index.html'){
            $this->error('你无操作权限','/admin/index.html');
        }
        //echo "<pre>";print_r($paths);exit;
        $this->assign('paths',$paths);
        //标题
        $title = $this->onode->get_title($path_info);
        $this->assign('title',$title);
        //左侧菜单
        $navlist = $this->onode->get_nodes('0',$path_info);
        foreach ($navlist as $key=>&$nav) {
            if(!in_array($nav['auth'],$user_auth)){
                unset($navlist[$key]);
            }
            if(!empty($nav['child'])){
                foreach ($nav['child'] as $kid=>$child) {
                    if(!in_array($child['auth'],$user_auth)){
                        unset($nav['child'][$kid]);
                    }
                }
            }
        }
        //echo "<pre>";print_r($navlist);exit;
        $this->assign('navlist',$navlist);
        //网站信息
        $this->osetting = new \app\admin\model\AdminSettings();
        $this->assign('settings',$this->osetting->get_settings());
    }
}