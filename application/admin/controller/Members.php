<?php
namespace app\admin\controller;
use app\base\Controller;
/**
* 
*/
class Members extends Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->member = new \app\admin\model\AdminMembers();
        $this->group  = new \app\admin\model\AdminGroups();
    }

    public function index(){
        //获取用户组
        $groups = $this->group->where(['is_use'=>'true'])->select();
        $tmp_groups = [];
        if(!empty($groups)){
            foreach ($groups as &$group) {
                $group = $group->toArray();
                $tmp_groups[$group['group_id']] = $group['name'];
            }
        }
        $members = $this->member->where(['disable'=>'false'])->field('login_name,email,member_id,group_id,create_time,is_use')->select();
        if(!empty($members)){
            foreach ($members as &$member) {
                $member = $member->toArray();
                $member['group'] = $tmp_groups[$member['group_id']];
                $member['is_use'] = $member['is_use']?'是':'否';
            }
        }
        $this->assign('groups',$groups);
        $this->assign('members',$members);
        return $this->view->fetch();
    }

    public function get_member(){
        $member_id = $this->request->post('memberid');
        $member = $this->member->where(['member_id'=>$member_id])->field('login_name,email,member_id,group_id,is_use,mobile')->find();
        if(empty($member)){
            return ajax_return([],'该用户不存在','error','1');
        }
        $member = $member->toArray();
        return ajax_return($member);
    }

    public function save_member(){
        $postData = $this->request->post();
        $now_time = time();
        $tmember = [
            'login_name'  => trim($postData['member']['name']),
            'email'       => $postData['member']['email'],
            'mobile'      => $postData['member']['phone'],
            'group_id'    => $postData['member']['group'],
            'is_use'      => $postData['member']['is_use'],
            'last_modify' => $now_time,
        ];
        if(isset($postData['member']['id'])&&!empty($postData['member']['id'])){
            $tmember['member_id'] = $postData['member']['id'];
            $amember = $this->member->where(['member_id'=>['neq',$tmember['member_id']],'login_name'=>$tmember['login_name']])->find();
            if(!empty($amember)){
                return ajax_return([],'用户名重复','error','1');
            }
            $this->member->update($tmember);
        }else{
            $tmember['create_time'] = $now_time;
            $amember = $this->member->where('login_name',$tmember['login_name'])->find();
            if(!empty($amember)){
                return ajax_return([],'用户名重复','error','1');
            }
            $tmember['password'] = create_password(config('init_password'),config('init_salt'));
            $tmember['register_ip'] = $this->request->server('REMOTE_ADDR');
            $member_id = $this->member->insertGetId($tmember);
            if(!$member_id){
                return ajax_return([],'保存失败','error','2');
            }
        }
        return ajax_return([]);
    }

}