<?php
namespace app\admin\controller;
use app\base\Controller;

/**
* 
*/
class Center extends Controller
{
    
    function __construct()
    {
        parent::__construct();
    }

    public function modify_pwd(){
        $this->assign('title','修改密码');
        $this->assign('paths',[['name'=>'个人中心']]);
        return $this->view->fetch();
    }

    public function save_pwd(){
        $postData = $this->request->post();
        foreach($postData['password'] as &$value){
            $value = trim($value);
        }
        $user = $this->user;
        $old_pwd = create_password($postData['password']['old'],config('init_salt'));
        if($old_pwd !== $user['password']){
            return ajax_return([],'旧密码错误','error','1');
        }
        if($postData['password']['new'] !== $postData['password']['chk']){
            return ajax_return([],'两次密码不匹配','error','2');
        }
        $new_pwd = create_password($postData['password']['new'],config('init_salt'));
        $obj_members = new \app\admin\model\AdminMembers();
        $obj_members->update(['member_id'=>$user['member_id'],'password'=>$new_pwd]);
        //echo "<pre>";print_r($user);exit;
        return ajax_return([]);
    }
}