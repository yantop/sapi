<?php
namespace app\admin\controller;
use app\base\Controller;
/**
* 
*/
class Groups extends Controller{
    
    function __construct(){
        parent::__construct();
        $this->group = new \app\admin\model\AdminGroups();
    }

    public function index(){
        $obj_user = new \app\admin\model\AdminMembers();
        $groups = $this->group->where(['disable'=>'false'])->select();
        if(!empty($groups)){
            foreach ($groups as &$group) {
                $group = $group->toArray();
                $group['is_use']      = ($group['is_use'] == 'true')?'是':'否';
                $group['last_modify'] = date('Y-m-d h:i:s',$group['last_modify']);
                $group['count']       = $obj_user->where(['group_id'=>$group['group_id']])->count();
                $group['type']        = ($group['type'] == 'member')?'前台用户':'系统管理员';
            }
        }
        $this->assign('groups',$groups);
        return $this->view->fetch();
    }

    public function get_auth(){
        $navlist = $this->onode->get_nodes('0');
        return ajax_return($navlist);
    }
    public function get_group(){
        $group_id = $this->request->post('groupid');
        $group = $this->group->where(['group_id'=>$group_id])->find();
        if(empty($group)){
            return ajax_return([],'该分组不存在','error','1');
        }
        $group = $group->toArray();
        $group['auth'] = unserialize($group['auth']);
        return ajax_return($group);
    }
    public function save_group(){
        $postData = $this->request->post();
        $now_time = time();
        $type = $postData['group']['type'];
        $tgroup = [ 
            'name'        => trim($postData['group']['name']),
            'is_use'      => $postData['group']['is_use'],
            'type'        => $postData['group']['type'],
            'last_modify' => $now_time,
        ];
        $tauth = [];
        if(isset($postData['group']['auth'][$type])){
            $auths = $postData['group']['auth'][$type];
            foreach($auths as $auth){
                if(is_array($auth)){
                    foreach ($auth as $one) {
                        $tauth[] = $one;
                    }
                }else{
                    $tauth[] = $auth;
                }
            }
        }
        $tgroup['auth'] = serialize($tauth);
        if(isset($postData['group']['id'])&&!empty($postData['group']['id'])){

            $tgroup['group_id'] = $postData['group']['id'];
            $agroup = $this->group->where(['group_id'=>['<>',$tgroup['group_id']],'name'=>$tgroup['name']])->find();
            if(!empty($agroup)){
                return ajax_return([],'组名不得重复','error','1');
            }
            $this->group->update($tgroup);
        }else{
            $agroup = $this->group->where('name',$tgroup['name'])->find();
            if(!empty($agroup)){
                return ajax_return([],'组名不得重复','error','1');
            }
            $tgroup['create_time'] = $now_time;
            
            $group_id = $this->group->insertGetId($tgroup);
            if(!$group_id){
                return ajax_return([],'保存失败','error','1');
            }
        }
        return ajax_return([]);
    }
    public function del_group(){
        $group_id = $this->request->post('groupid');
        $obj_user = new \app\admin\model\AdminMembers();
        $count = $obj_user->where(['group_id'=>$group_id])->count();
        if(!empty($count)){
            return ajax_return([],'该分组中存在用户不能删除','error','1');
        }
        $this->group->where(['group_id'=>$group_id])->delete();
        return ajax_return([]);
    }

}