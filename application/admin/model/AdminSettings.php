<?php
namespace app\admin\model;
use think\Model;
/**
* 
*/
class AdminSettings extends Model
{
    public function get_settings(){
        $settings = $this->where('disable','false')->select();
        $web_msg = [];
        if(!empty($settings)){
            foreach ($settings as $key => $setting) {
                $web_msg[$setting['key']] = stripcslashes($setting['value']);
            }
        }
        return $web_msg;
    }
}