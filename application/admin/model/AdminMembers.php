<?php
namespace app\admin\model;
use think\Model;
/**
* 
*/
class AdminMembers extends Model
{
    public function get_user($username = null){
        if(empty($username)){
            return false;
        }
        $tmember = $this->where(['login_name'=>$username,'disable'=>'false'])->find();
        if(empty($tmember)){
            return false;
        }
        $tmember->group;
        $tmember = $tmember->toArray();
        return $tmember;
    }
    public function group(){
        return $this->hasOne('AdminGroups','group_id','group_id');
    }
}