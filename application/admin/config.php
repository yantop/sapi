<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
/**
 * 配置文件
 */
use \think\Request;

$basename = Request::instance()->root();
$webroot  = str_replace('\\','/',$basename);
if (pathinfo($basename, PATHINFO_EXTENSION) == 'php') {
    $basename = trim(str_replace('\\','/',dirname($basename)),'/');
    $basename = $basename ? '/'.$basename : '';
}

return [
    'template'  =>  [
        'layout_on'     =>  true,
        'layout_name'   =>  'layout',
    ],
    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => str_replace('\\','/',APP_PATH) . 'base/view/templates/jump.html',
    'dispatch_error_tmpl'    => str_replace('\\','/',APP_PATH) . 'base/view/templates/jump.html',
    // +----------------------------------------------------------------------
    // | 异常及错误设置
    // +----------------------------------------------------------------------

    // 异常页面的模板文件
    'exception_tmpl'         => str_replace('\\','/',APP_PATH) . 'base/view/templates/throw.html',

    // 错误显示信息,非调试模式有效
    'error_message'          => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',
];