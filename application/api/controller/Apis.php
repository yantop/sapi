<?php
namespace app\api\controller;
use app\base\Controller;
use think\Db;
/**
* 
*/
class Apis extends Controller
{
    
    function __construct(){
        parent::__construct();
        $this->oproject = new \app\apiweb\model\Classify();
        $this->apilist  = new \app\apiweb\model\Apilist();
        $this->odetail = new \app\apiweb\model\Apidetail();
        $this->oparam   = new \app\apiweb\model\Apiparam();
    }

    public function get_data($obj,$postData,$field = '',$filter = [],$order,$orderBy = 'desc',$groupBy = null){
        $limit     = $postData['limit']?:20;
        $offset    = $postData['offset']?:0;
        $orderBy   = $postData['order']?:$orderBy;
        $orderName = $postData['sort']?:$order;
        $data = [];
        $dataCount = 0;
        if(empty($groupBy)){
            $data = $obj->where($filter)->limit($offset,$limit)->order($orderName,$orderBy)->field($field)->select();
            $dataCount = $obj->where($filter)->count();
        }else{
            $data = $obj->where($filter)->limit($offset,$limit)->order($orderName,$orderBy)->field($field)->group($groupBy)->select();
            $dataCount = $obj->where($filter)->group($groupBy)->count();
        }
        if($dataCount == 0){
            $data = [];
        }
        return ['rows'=>$data,'total'=>$dataCount];
    }

    public function get_apis(){
        $postData  = $this->request->post();
        $scolumn  = $postData['scolumn']?:'';
        $search   = trim($postData['search'])?trim($postData['search']):'';
        $filter   = ['status' => '2'];
        if(!empty($search)){
            $filter[$scolumn] = ['like',"%$search%"];
        }
        $data = $this->get_data($this->odetail,$postData,'URI,version,mtime,description,listid,id as d_id',$filter,'ctime');
        if(!empty($data['rows'])){
            foreach ($data['rows'] as &$detail) {
                $detail = $detail->toArray();
                $alist = $this->apilist->where('id',$detail['listid'])->find();
                $alist = $alist->toArray();
                $detail['apiname'] = $alist['apiname'];
                $detail['mtime'] = date('Y-m-d H:i:s',$detail['mtime']);
            }
        }
        echo json_encode($data);
    }

    public function index(){
        $projects = [];
        $projects = $this->oproject->where('pid','0')->select();
        if(!empty($projects)){
            foreach ($projects as $key=>&$project) {
                $project = $project->toArray();
            }
        }
        $this->assign('projects',$projects);
        return $this->view->fetch();
    }
    public function save_api(){
        $postData = $this->request->post();
        //echo "<pre>";print_r($postData);exit;
        //list表处理
        $tlist = [
            'apiname'  => $postData['api']['name'],
            'classify' => $postData['api']['category'],
            'envid'    => '1',
            'initid'   => '0',
            'status'   => '1',
        ];
        Db::startTrans();
        if(isset($postData['api']['id'])&&!empty($postData['api']['id'])){
            $this->apilist->where('id',$postData['api']['id'])->update($tlist);
            $tlist['id'] = $postData['api']['id'];
        }else{
            $list_id = $this->apilist->insertGetId($tlist);
            if(empty($list_id)){
                Db::rollback();
                return ajax_return([],'保存失败','error','2');
            }else{
                $tlist['id'] = $list_id;
            }
        }
        //detail表处理
        $now_time = time();
        $local_url = parse_url($postData['api']['local']);
        $tdetail = [
            'listid'      => $tlist['id'],
            'URI'         => $local_url['path'],
            'version'     => $postData['api']['version'],
            'gateway'     => $postData['api']['gateway'],
            'local'       => $postData['api']['local'],
            'goback'      => $postData['api']['response'],
            'mtime'       => $now_time,
            'description' => $postData['api']['description'],
            'author'      => '1',
            'editor'      => '1',
            'network'     => '1',
            'auth'        => '0',
            'status'      => '2',
        ];
        $tmp_detail = $this->odetail->where('listid',$tlist['id'])->find();
        if(!empty($tmp_detail)){
            $this->odetail->where('listid',$tlist['id'])->update($tdetail);
            $tdetail['id'] = $tmp_detail['id'];
        }else{
            $tdetail['ctime'] = $now_time;
            $detail_id = $this->odetail->insertGetId($tdetail);
            if(empty($detail_id)){
                Db::rollback();
                return ajax_return([],'保存失败','error','4');
            }else{
                $tdetail['id'] = $detail_id;
            }
        }
        //param表处理
        
        $param_headers = [];
        if(!empty($postData['api']['param']['header'])){
            $header = $postData['api']['param']['header'];
            foreach ($header['field'] as $key => $param) {
                $tmp_header['field']   = $param;
                $tmp_header['type']    = $header['type'][$key];
                $tmp_header['must']    = $header['must'][$key];
                $tmp_header['des']     = $header['des'][$key];
                $tmp_header['default'] = $header['default'][$key];
                $param_headers[] = $tmp_header;
            }//header参数
        }
        $errors = [];
        if(!empty($postData['api']['error'])){
            $error = $postData['api']['error'];
            foreach ($error['status'] as $key => $code) {
                $tmp_error['status'] = $code;
                $tmp_error['des'] = $error['des'][$key];
                $errors[] = $tmp_error;
            }//error参数
        }
        $type = $this->oparam->apiType[$postData['api']['type']];
        $other = $postData['api']['param']['other'];
        $others = [];
        foreach ($other['field'] as $key=>$param) {
            $tmp_other['field']   = $param;
            $tmp_other['type']    = $other['type'][$key];
            $tmp_other['must']    = $other['must'][$key];
            $tmp_other['des']     = $other['des'][$key];
            $tmp_other['default'] = $other['default'][$key];
            $others[$type][] = $tmp_other;
        }//request参数
        
        $responses = [];
        if(!empty($postData['api']['param']['response'])){
            $response = $postData['api']['param']['response'];
            foreach ($response['field'] as $key => $param) {
                $tmp_res['field']   = $param;
                $tmp_res['type']    = $response['type'][$key];
                $tmp_res['must']    = $response['must'][$key];
                $tmp_res['des']     = $response['des'][$key];
                $tmp_res['default'] = $response['default'][$key];
                $responses[$type][] = $tmp_res;
            }//response参数
        }
        $tparam = [
            'header'     => json_encode($param_headers),
            'request'    => json_encode($others),
            'statuscode' => json_encode($errors),
            'response'   => json_encode($responses),
            'type'       => $postData['api']['type'],
            'detailid'   => $tdetail['id'],
        ];
        if(!empty($tmp_detail)){
            $this->oparam->where('detailid',$tdetail['id'])->update($tparam);
        }else{
            $param_id = $this->oparam->insertGetId($tparam);
            if(empty($param_id)){
                Db::rollback();
                return ajax_return([],'保存失败','error','6');
            }
        }
        Db::commit();
        //echo "<pre>";print_r($postData);exit;
        return ajax_return();
    }

    public function del_api(){
        $api_id = $this->request->post('apiid');
        $detail = $this->odetail->where('id',$api_id)->find();
        if(empty($detail)){
            return ajax_return([],'未查询到该接口信息','error','1');
        }
        $detail = $detail->toArray();
        $status = $this->odetail->where('id',$api_id)->delete();
        if($status){
            $this->oparam->where('detailid',$detail['id'])->delete();
            $this->apilist->where('id',$detail['listid'])->delete();
            return ajax_return();
        }else{
            return ajax_return([],'删除失败','error','2');
        }
    }

    public function get(){
        $api_id = $this->request->post('apiid');
        $detail = $this->odetail->where('id',$api_id)->find();
        if(empty($detail)){
            return ajax_return([],'未查询到该接口信息','error','1');
        }
        $detail = $detail->toArray();
        $param  = $this->oparam->where('detailid',$detail['id'])->find();
        $param['apitype'] = $this->oparam->apiType[$param['type']];
        $list   = $this->apilist->where('id',$detail['listid'])->find();
        $category = $this->oproject->where('id',$list['classify'])->find();
        if(!empty($category)){
            $category = $category->toArray();
            $list['project'] = $category['pid'];
            $obj_projects = new \app\api\controller\Projects();
            $categories = $obj_projects->get_data($category['pid']);
            $list['categories'] = $categories;
        }
        return ajax_return(['detail'=>$detail,'list'=>$list,'param'=>$param]);
    }
}