/*
Navicat MySQL Data Transfer

Source Server         : phpstudy-mysql
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : sapi

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-08-08 13:06:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for yj_admin_groups
-- ----------------------------
DROP TABLE IF EXISTS `yj_admin_groups`;
CREATE TABLE `yj_admin_groups` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户组ID',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '组名',
  `type` enum('admin','member') DEFAULT 'admin',
  `is_use` enum('false','true') DEFAULT NULL COMMENT '是否启用',
  `create_time` int(10) NOT NULL,
  `last_modify` int(10) NOT NULL,
  `disable` enum('false','true') DEFAULT 'false',
  `auth` longtext,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_admin_groups
-- ----------------------------
INSERT INTO `yj_admin_groups` VALUES ('2', '超级管理员', 'admin', 'true', '1501825584', '1501831696', 'false', 'a:10:{i:0;s:8:\"2YKIZVGW\";i:1;s:8:\"OQ8BIL2Y\";i:2;s:8:\"WBLGTM1E\";i:3;s:8:\"WXKPSLRY\";i:4;s:8:\"VEXI66NB\";i:5;s:8:\"7QOEU9O1\";i:6;s:8:\"9YQUDEG6\";i:7;s:8:\"6SPW8BBO\";i:8;s:8:\"YHHGPMWZ\";i:9;s:8:\"SKBN7DQU\";}');
INSERT INTO `yj_admin_groups` VALUES ('4', '普通用户', 'member', 'true', '1501827027', '1501838401', 'false', 'a:0:{}');
INSERT INTO `yj_admin_groups` VALUES ('5', '接口管理', 'admin', 'true', '1502095230', '1502095230', 'false', 'a:3:{i:0;s:8:\"6SPW8BBO\";i:1;s:8:\"YHHGPMWZ\";i:2;s:8:\"SKBN7DQU\";}');

-- ----------------------------
-- Table structure for yj_admin_images
-- ----------------------------
DROP TABLE IF EXISTS `yj_admin_images`;
CREATE TABLE `yj_admin_images` (
  `image_id` char(32) NOT NULL,
  `name` varchar(255) NOT NULL,
  `size` int(10) NOT NULL,
  `type` varchar(10) NOT NULL,
  `path` varchar(255) NOT NULL,
  `create_time` int(10) NOT NULL,
  `last_modify` int(10) NOT NULL,
  `storeway` varchar(20) NOT NULL,
  `md5` char(40) NOT NULL,
  `height` int(8) NOT NULL,
  `width` int(8) NOT NULL,
  `disable` enum('false','true') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_admin_images
-- ----------------------------
INSERT INTO `yj_admin_images` VALUES ('ec7a9466cb9e802981df07ac196e438c', 'd1160924ab18972b4f4877ebe5cd7b899e510aa4.jpg', '261049', 'image/jpeg', '20170707/f7f912d8088c5dac368ce6e55bd05510.jpg', '1499413261', '1499413261', 'local', '16377d3c8b744b26191fac348e941e15f24a431b', '800', '800', 'false');
INSERT INTO `yj_admin_images` VALUES ('564113369b494083329e6dce31d4d047', 'logo.png', '1438', 'image/png', '20170707/194b8e05c5cdd2b9726db2877ca5ec78.png', '1499413763', '1499413763', 'local', '9bc59e369d80a2c234951620c2c0ee78077db3f7', '128', '128', 'false');

-- ----------------------------
-- Table structure for yj_admin_members
-- ----------------------------
DROP TABLE IF EXISTS `yj_admin_members`;
CREATE TABLE `yj_admin_members` (
  `member_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `group_id` int(10) NOT NULL COMMENT '用户组ID',
  `real_name` varchar(50) DEFAULT NULL COMMENT '实名',
  `nick_name` varchar(100) DEFAULT NULL COMMENT '昵称',
  `login_name` varchar(255) NOT NULL COMMENT '用户名（登录名）',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `email` varchar(255) NOT NULL COMMENT '邮箱',
  `brithdate` int(10) DEFAULT NULL COMMENT '出生年月日',
  `sex` enum('M','F') DEFAULT 'M' COMMENT '性别',
  `qq` varchar(50) DEFAULT NULL COMMENT 'qq号',
  `wechat` varchar(50) DEFAULT NULL COMMENT '微信号',
  `weibo` varchar(50) DEFAULT NULL COMMENT '微博号',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
  `head_photo` varchar(255) DEFAULT NULL COMMENT '头像',
  `verify_email` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT '邮箱是否验证',
  `verify_mobile` enum('N','Y') DEFAULT 'N' COMMENT '手机号是否验证',
  `source` varchar(255) NOT NULL DEFAULT '' COMMENT '来源',
  `register_ip` varchar(20) DEFAULT NULL COMMENT '注册IP',
  `is_use` enum('false','true') DEFAULT 'true',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `last_modify` int(10) NOT NULL COMMENT '更新时间',
  `disable` enum('false','true') NOT NULL DEFAULT 'false',
  `idcard` varchar(20) DEFAULT NULL COMMENT '身份证号',
  PRIMARY KEY (`member_id`),
  UNIQUE KEY `idnex_loginname` (`login_name`) USING BTREE,
  KEY `index_group_id` (`group_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_admin_members
-- ----------------------------
INSERT INTO `yj_admin_members` VALUES ('1', '5', null, null, 'yantop', 'cc40b64fe7228a8e6a7d09e731900198', 'yantop@foxmail.com', null, 'M', null, null, null, '18721496048', null, 'N', 'N', '', '::1', 'true', '1501836464', '1502095242', 'false', null);
INSERT INTO `yj_admin_members` VALUES ('2', '2', null, null, 'admin', 'cc40b64fe7228a8e6a7d09e731900198', 'root@admin.com', null, 'M', null, null, null, '13112332112', null, 'N', 'N', '', '::1', 'true', '1501837554', '1501837554', 'false', null);
INSERT INTO `yj_admin_members` VALUES ('3', '4', null, null, 'qwe', 'cc40b64fe7228a8e6a7d09e731900198', 'qwe@11.com', null, 'M', null, null, null, '13212312311', null, 'N', 'N', '', '::1', 'true', '1501837731', '1501837731', 'false', null);
INSERT INTO `yj_admin_members` VALUES ('4', '4', null, null, '222', 'cc40b64fe7228a8e6a7d09e731900198', '222@qq.com', null, 'M', null, null, null, '13112345678', null, 'N', 'N', '', '::1', 'true', '1501837776', '1501837776', 'false', null);

-- ----------------------------
-- Table structure for yj_admin_nodes
-- ----------------------------
DROP TABLE IF EXISTS `yj_admin_nodes`;
CREATE TABLE `yj_admin_nodes` (
  `node_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `auth` char(8) NOT NULL DEFAULT '' COMMENT '权限标识',
  `link` varchar(255) DEFAULT NULL,
  `icon` varchar(50) NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `order` int(4) NOT NULL DEFAULT '1',
  `create_time` int(10) NOT NULL,
  `last_modify` int(10) NOT NULL,
  `is_use` enum('false','true') NOT NULL DEFAULT 'true',
  `disable` enum('false','true') NOT NULL DEFAULT 'false',
  PRIMARY KEY (`node_id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_admin_nodes
-- ----------------------------
INSERT INTO `yj_admin_nodes` VALUES ('1', '系统管理', 'OQ8BIL2Y', '#', 'ion-gear-a', '0', '2', '1498810030', '1501811012', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('12', '角色管理', '7QOEU9O1', 'admin/groups/index.html', '', '11', '1', '1499133451', '1501811053', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('11', '用户管理', 'VEXI66NB', '#', 'ion-person-stalker', '0', '3', '1499133260', '1501811028', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('8', '节点管理', 'WBLGTM1E', 'admin/nodes/index.html', '', '1', '1', '1499132742', '1501811017', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('13', '用户列表', '9YQUDEG6', 'admin/members/index.html', '', '11', '2', '1499133609', '1501811033', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('14', '网站概况', '2YKIZVGW', 'admin/index/index.html', 'fa fa-home', '0', '1', '1499134077', '1501810983', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('23', '文件上传', '', 'example/upload/index.html', '', '22', '1', '1499223568', '1499417252', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('17', '项目管理', '6SPW8BBO', '#', 'ion-navicon-round', '0', '4', '1499153185', '1501811089', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('18', '接口列表', 'SKBN7DQU', 'api/apis/index.html', '', '17', '2', '1499153311', '1501811037', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('20', '项目列表', 'YHHGPMWZ', 'api/projects/index.html', '', '17', '1', '1499157116', '1501811042', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('21', '站点设置', 'WXKPSLRY', 'admin/web/index.html', '', '1', '2', '1499157222', '1501811023', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('24', 'Excel处理', '', 'tools/excel/index.html', '', '22', '2', '1499223699', '1499223801', 'true', 'false');
INSERT INTO `yj_admin_nodes` VALUES ('25', '二维码生成', '', 'tools/qrcodes/index.html', '', '22', '1', '1499223884', '1500862338', 'true', 'false');

-- ----------------------------
-- Table structure for yj_admin_settings
-- ----------------------------
DROP TABLE IF EXISTS `yj_admin_settings`;
CREATE TABLE `yj_admin_settings` (
  `setting_id` int(10) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  `disable` enum('false','true') DEFAULT 'false',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_admin_settings
-- ----------------------------
INSERT INTO `yj_admin_settings` VALUES ('1', 'admin_theme', 'type\\-a\\/theme\\-ocean\\.min\\.css', 'false');
INSERT INTO `yj_admin_settings` VALUES ('2', 'web_name', '接口管理', 'false');
INSERT INTO `yj_admin_settings` VALUES ('3', 'web_logo', '564113369b494083329e6dce31d4d047', 'false');
INSERT INTO `yj_admin_settings` VALUES ('4', 'web_intro', 'yyyy', 'false');
INSERT INTO `yj_admin_settings` VALUES ('5', 'web_icpid', '京\\-2342243\\-A号', 'false');
INSERT INTO `yj_admin_settings` VALUES ('6', 'admin_under', '2391023192', 'false');
INSERT INTO `yj_admin_settings` VALUES ('7', 'site_under', '5485934083054', 'false');

-- ----------------------------
-- Table structure for yj_apidetail
-- ----------------------------
DROP TABLE IF EXISTS `yj_apidetail`;
CREATE TABLE `yj_apidetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '接口id',
  `listid` int(11) DEFAULT NULL COMMENT '列表接口id',
  `URI` varchar(200) DEFAULT NULL COMMENT '接口URI部分',
  `version` varchar(20) DEFAULT NULL COMMENT '版本',
  `gateway` varchar(200) DEFAULT NULL COMMENT 'gateway地址',
  `local` varchar(200) DEFAULT NULL COMMENT 'localapi本地开发地址',
  `description` text COMMENT '接口描述',
  `author` int(11) DEFAULT NULL COMMENT '开发者',
  `editor` varchar(100) DEFAULT NULL COMMENT '编辑人',
  `network` tinyint(1) DEFAULT '1' COMMENT '网络访问(1内网,2外网)',
  `goback` text COMMENT '想要示例',
  `auth` tinyint(1) DEFAULT '0' COMMENT '认证方式(1session认证,2secret/token认证)',
  `status` tinyint(2) DEFAULT '2' COMMENT '接口状态(1已审核,2待审核,3废弃,4删除,5已拒绝)',
  `mtime` int(11) DEFAULT NULL COMMENT '修改时间',
  `ctime` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_apidetail
-- ----------------------------
INSERT INTO `yj_apidetail` VALUES ('16', '22', '/testone', 'v1', 'http://www.baidu.com', 'http://192.168.1.1/testone', '这是一个测试api', '1', '1', '1', '{\r\n    \"success\": 1,\r\n    \"message\": \"\",\r\n    \"data\": [],\r\n    \"head\": {\r\n        \"count\": 0,\r\n        \"isover\": 0\r\n    }\r\n}', '0', '2', '1502167636', '1502159794');
INSERT INTO `yj_apidetail` VALUES ('18', '24', '/test/testsecond', 'V1', 'http://www.baidu.com/testsecond', 'http://localhost:8081/test/testsecond', '这是第二个测试，没什么的，，，', '1', '1', '1', '{\r\n    \"success\": 1,\r\n    \"message\": \"\",\r\n    \"data\": [],\r\n    \"head\": {\r\n        \"count\": 0,\r\n        \"isover\": 0\r\n    }\r\n}', '0', '2', '1502160710', '1502160145');

-- ----------------------------
-- Table structure for yj_apilist
-- ----------------------------
DROP TABLE IF EXISTS `yj_apilist`;
CREATE TABLE `yj_apilist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `envid` int(11) DEFAULT NULL COMMENT '环境id',
  `classify` int(11) DEFAULT NULL COMMENT '接口子分类',
  `apiname` varchar(200) DEFAULT NULL COMMENT '资源名',
  `initid` int(11) DEFAULT '0' COMMENT '初始化id',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态(1正常,2删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_apilist
-- ----------------------------
INSERT INTO `yj_apilist` VALUES ('22', '1', '12', ' 测试同步', '0', '1');
INSERT INTO `yj_apilist` VALUES ('24', '1', '12', '测试2啊', '0', '1');

-- ----------------------------
-- Table structure for yj_apiparam
-- ----------------------------
DROP TABLE IF EXISTS `yj_apiparam`;
CREATE TABLE `yj_apiparam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detailid` int(11) DEFAULT NULL COMMENT '接口id',
  `type` varchar(20) DEFAULT NULL COMMENT '请求方式(1GET,2POST,3PUT,4DELETE)',
  `header` longtext COMMENT 'header参数',
  `request` longtext COMMENT '请求参数',
  `response` longtext COMMENT '响应参数',
  `statuscode` longtext COMMENT '状态码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_apiparam
-- ----------------------------
INSERT INTO `yj_apiparam` VALUES ('14', '16', '1', '[{\"field\":\"hello\",\"type\":\"string\",\"must\":\"true\",\"des\":\"hello\\u53c2\\u6570\\uff0c\\u4f60\\u597d\",\"default\":\"world\"},{\"field\":\"php\",\"type\":\"string\",\"must\":\"true\",\"des\":\"php\\u53c2\\u6570\\uff0ctest\",\"default\":\"\"}]', '{\"GET\":[{\"field\":\"userid\",\"type\":\"string\",\"must\":\"true\",\"des\":\"\\u7528\\u6237id\",\"default\":\"\"},{\"field\":\"action\",\"type\":\"string\",\"must\":\"true\",\"des\":\"\\u52a8\\u4f5c\",\"default\":\"eat\"}]}', '{\"GET\":[{\"field\":\"success\",\"type\":\"string\",\"must\":\"true\",\"des\":\"\\u662f\\u5426\\u8c03\\u7528\\u6210\\u529f 1\\u8bf7\\u6c42\\u6210\\u529f,0\\u8bf7\\u6c42\\u5931\\u8d25\",\"default\":\"\"},{\"field\":\"data\",\"type\":\"array\",\"must\":\"true\",\"des\":\"\\u8fd4\\u56de\\u6570\\u636e\",\"default\":\"\"},{\"field\":\"message\",\"type\":\"string\",\"must\":\"true\",\"des\":\"\\u8fd4\\u56de\\u4fe1\\u606f\",\"default\":\"\"},{\"field\":\"err_code\",\"type\":\"int\",\"must\":\"false\",\"des\":\"\\u9519\\u8bef\\u7801\",\"default\":\"\"}]}', '[{\"status\":\"101\",\"des\":\"\\u53c2\\u6570\\u4e3a\\u7a7a\\uff01\"},{\"status\":\"102\",\"des\":\"\\u53c2\\u6570\\u9519\\u8bef\"}]');
INSERT INTO `yj_apiparam` VALUES ('15', '18', '2', '[{\"field\":\"\",\"type\":\"string\",\"must\":\"true\",\"des\":\"\",\"default\":\"\"}]', '{\"POST\":[{\"field\":\"lang\",\"type\":\"string\",\"must\":\"false\",\"des\":\"\\u8bed\\u8a00\",\"default\":\"php\"},{\"field\":\"create\",\"type\":\"string\",\"must\":\"false\",\"des\":\"\\u521b\\u5efa\",\"default\":\"sapi\"},{\"field\":\"time\",\"type\":\"int\",\"must\":\"false\",\"des\":\"\\u65f6\\u95f4\",\"default\":\"\"},{\"field\":\"user\",\"type\":\"string\",\"must\":\"true\",\"des\":\"\\u7528\\u6237\",\"default\":\"\"}]}', '{\"POST\":[{\"field\":\"success\",\"type\":\"string\",\"must\":\"true\",\"des\":\"\\u662f\\u5426\\u8c03\\u7528\\u6210\\u529f 1\\u8bf7\\u6c42\\u6210\\u529f,0\\u8bf7\\u6c42\\u5931\\u8d25\",\"default\":\"\"},{\"field\":\"data\",\"type\":\"array\",\"must\":\"true\",\"des\":\"\\u8fd4\\u56de\\u6570\\u636e\",\"default\":\"\"},{\"field\":\"message\",\"type\":\"string\",\"must\":\"true\",\"des\":\"\\u8fd4\\u56de\\u4fe1\\u606f\",\"default\":\"\"}]}', '[]');

-- ----------------------------
-- Table structure for yj_classify
-- ----------------------------
DROP TABLE IF EXISTS `yj_classify`;
CREATE TABLE `yj_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `classifyname` varchar(100) DEFAULT NULL COMMENT '分类名称',
  `pid` int(11) DEFAULT '0' COMMENT '父级id',
  `description` varchar(600) DEFAULT NULL COMMENT '分类描述',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  `creator` int(11) DEFAULT NULL COMMENT '添加人',
  `leader` varchar(300) DEFAULT NULL COMMENT '负责人',
  `status` tinyint(2) DEFAULT '1' COMMENT '分类状态(1正常,2删除)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yj_classify
-- ----------------------------
INSERT INTO `yj_classify` VALUES ('10', 'test', '9', 'test', '1501490311', '1', '1', '1');
INSERT INTO `yj_classify` VALUES ('11', '第一个测试项目', '0', '本项目用于测试', '1502159198', '1', '1', '1');
INSERT INTO `yj_classify` VALUES ('12', 'testone', '11', '这是一个测试测试分类', '1502159237', '1', '1', '1');
INSERT INTO `yj_classify` VALUES ('13', 'testtwo', '11', '这是第二个测试分类', '1502159264', '1', '1', '1');
INSERT INTO `yj_classify` VALUES ('15', '第二个测试项目', '0', '这个是第二个测试项目啊', '1502159328', '1', '1', '1');
INSERT INTO `yj_classify` VALUES ('16', '商品类', '15', '与商品相关的API接口', '1502159449', '1', '1', '1');
INSERT INTO `yj_classify` VALUES ('17', '订单类', '15', '与订单相关的API', '1502159483', '1', '1', '1');
